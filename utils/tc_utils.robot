*** Settings ***
Library         RequestsLibrary
Library           String
Variables       D:/gorest/gorest/src/gorest/utils/variables.py

*** Keywords ***
Create session without authentication
    ${headers}=  Create Dictionary    accept=${accept_type_json}   Content-Type=${accept_type_json}
    RequestsLibrary.Create Session    session    ${base_url}       headers=${headers}

Check Response Status
        [Arguments]     ${resp}
        Request Should Be Successful       ${resp}

Convert to JSON
        [Arguments]  ${resp}
        ${resp_json}   Set Variable    ${resp.json()}
        Set Test Variable       ${resp_json}
		
Tc-logger
        [Documentation]   it will log in console as well
        [Arguments]      ${message}
        log       ${message}       formatter=repr
        log to console    ${message}

Generate a token
    ${accessToken}=     Token Generator
    ${bearerToken}=    catenate    Bearer    ${accessToken}
	Set Suite Variable  ${bearerToken}

Create session
    ${headers}=  Create Dictionary    Authorization=Bearer d3f911f31f29b9b8e9cc2ee19c42bd7e6420e2bc72ebeecf749f113617d199cf      accept=${accept_type_json}   Content-Type=application/json; charset=utf-8
    RequestsLibrary.Create Session    session    ${base_url}        headers=${headers}