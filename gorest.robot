*** Settings ***
Library         RequestsLibrary
Library         jsonschema
Resource        D:/gorest/gorest/src/gorest/utils/tc_utils.robot
Variables       D:/gorest/gorest/src/gorest/utils/variables.py
Suite Setup           Schema Format

*** Test Cases ***
Get User Details and Verify REST service without authentication
    [Documentation]   Return the user details
    Create session without authentication
    ${resp}=   GET On Session  url=/public/v2/users    alias=session
    Check Response Status    ${resp}
    Status Should Be    200    ${resp}
    Convert to JSON    ${resp}
	Tc-logger     ${resp_json}

Verify response has pagination
    [Documentation]   To verify the pagination details in the response
    Create session without authentication
    ${resp}=   GET On Session  url=/public/v1/users    alias=session
    Check Response Status    ${resp}
    Status Should Be    200    ${resp}
    Convert to JSON    ${resp}
	Tc-logger     ${resp_json}
	${pages}   Set Variable    ${resp_json["meta"]["pagination"]["pages"]}
	Tc-logger     Response has ${pages} pages


Verify Response Data has email address
    [Documentation]   To verify the email exists in the response
    Create session without authentication
    ${resp}=   GET On Session  url=/public/v1/users    alias=session
    Check Response Status    ${resp}
    Status Should Be    200    ${resp}
    Convert to JSON    ${resp}
	Tc-logger     ${resp_json}
	FOR  ${item}    IN    @{resp_json['data']}
        ${email}   Set Variable    ${item['email']}
        Tc-logger    ${email}
        Should Not Be Empty      ${email}
    END

Verify all entries on list data have similar attributes
    [Documentation]   verifying all the elements in the lists are same
    ${duplicate_list} =    Create List	    11  11  11
    ${iter_count}    Set Variable    ${0}    #${}
    ${count}=    Get length    ${duplicate_list}
    FOR  ${item}    IN    @{duplicate_list}
        Run Keyword If    ${duplicate_list[0]}!=${item}    Exit For Loop
        ${iter_count}=    Evaluate    ${iter_count} + 1
    END
    Tc-logger    ${iter_count}
    Run Keyword If    "${count}" =="${iter_count}"    Tc-logger    All the elements in the list are same
    ...     ELSE IF    "${count}" !="${iter_count}"    Tc-logger    List has different elements

Verify HTTP response codes
    [Documentation]   verifying http response codes
    Create session without authentication
    ${resp}=   GET On Session  url=/public-api/users    alias=session    expected_status=200
    ${resp}=   GET On Session  url=/public-api/user    alias=session    expected_status=404

Verify response has Valid Json Data
    [Documentation]   verifying the json schema
    Create session without authentication
    ${resp}=   GET On Session  url=/public/v2/users    alias=session
    Check Response Status    ${resp}
    Status Should Be    200    ${resp}
    Convert to JSON    ${resp}
	Tc-logger     ${resp_json}
	Evaluate            jsonschema.validate(instance=${resp_json}, schema=${schema})

Create New User
    [Documentation]   Create new user
    Create session
    ${resp}=   POST On Session  url=/public/v2/users?email=bhargavi.viruthapuri@gmail.com&name=kavi1&gender=female&status=active    alias=session
    Check Response Status    ${resp}
    Status Should Be    201    ${resp}
    Convert to JSON    ${resp}
	Tc-logger     ${resp_json}
	${id_created}    Set Variable    ${resp_json["id"]}
	Set Suite Variable  ${id_created}
	Tc-logger     ${id_created}

Modify User Details For Single Field
    [Documentation]   Modify user details for single field using patch
    Create session
    ${resp}=   PATCH On Session  url=/public/v2/users/${id_created}    alias=session    data={"email":"abc4@gmail.com"}
    Tc-logger     ${resp}
    Check Response Status    ${resp}
    Status Should Be    200    ${resp}
    Convert to JSON    ${resp}
	Tc-logger     ${resp_json}

Modify User Details For Multiple Fields
    [Documentation]   Modify user details for multiple fields using put
    Create session
	${resp}=   PUT On Session  url=/public/v2/users/${id_created}    alias=session    data={"name": "kavi3","email": "abc24@gmail.com","gender": "female","status": "active"}
    Tc-logger     ${resp}
    Check Response Status    ${resp}
    Status Should Be    200    ${resp}
    Convert to JSON    ${resp}
	Tc-logger     ${resp_json}
	
Delete User Details
    [Documentation]   delete user details
    Create session
    ${resp}=   DELETE On Session  url=/public/v2/users/${id_created}    alias=session
    Tc-logger     ${resp}
    Check Response Status    ${resp}
    Status Should Be    204    ${resp}

*** Keywords ***
Schema Format
    ${schema_str}=     Catenate   SEPARATOR=
...       {
...          "$schema": "http://json-schema.org/draft-04/schema#",
...          "type": "array",
...          "items": [
...            {
...              "type": "object",
...              "properties": {
...                "email": {
...                  "type": "string"
...                },
...                "gender": {
...                  "type": "string"
...                },
...                "id": {
...                  "type": "integer"
...                },
...                "name": {
...                  "type": "string"
...                },
...                "status": {
...                  "type": "string"
...                }
...              },
...              "required": [
...                "email",
...                "gender",
...                "id",
...                "name",
...                "status"
...              ]
...            },
...            {
...              "type": "object",
...              "properties": {
...                "email": {
...                  "type": "string"
...                },
...                "gender": {
...                  "type": "string"
...                },
...                "id": {
...                  "type": "integer"
...                },
...                "name": {
...                  "type": "string"
...                },
...                "status": {
...                  "type": "string"
...                }
...              },
...              "required": [
...                "email",
...                "gender",
...                "id",
...                "name",
...                "status"
...              ]
...            },
...            {
...              "type": "object",
...              "properties": {
...                "email": {
...                  "type": "string"
...                },
...                "gender": {
...                  "type": "string"
...                },
...                "id": {
...                  "type": "integer"
...                },
...                "name": {
...                  "type": "string"
...                },
...                "status": {
...                  "type": "string"
...                }
...              },
...              "required": [
...                "email",
...                "gender",
...                "id",
...                "name",
...                "status"
...              ]
...            },
...            {
...              "type": "object",
...              "properties": {
...                "email": {
...                  "type": "string"
...                },
...                "gender": {
...                  "type": "string"
...                },
...                "id": {
...                  "type": "integer"
...                },
...                "name": {
...                  "type": "string"
...                },
...                "status": {
...                  "type": "string"
...                }
...              },
...              "required": [
...                "email",
...                "gender",
...                "id",
...                "name",
...                "status"
...              ]
...            },
...            {
...              "type": "object",
...              "properties": {
...                "email": {
...                  "type": "string"
...                },
...                "gender": {
...                  "type": "string"
...                },
...                "id": {
...                  "type": "integer"
...                },
...                "name": {
...                  "type": "string"
...                },
...                "status": {
...                  "type": "string"
...                }
...              },
...              "required": [
...                "email",
...                "gender",
...                "id",
...                "name",
...                "status"
...              ]
...            },
...            {
...              "type": "object",
...              "properties": {
...                "email": {
...                  "type": "string"
...                },
...                "gender": {
...                  "type": "string"
...                },
...                "id": {
...                  "type": "integer"
...                },
...                "name": {
...                  "type": "string"
...                },
...                "status": {
...                  "type": "string"
...                }
...              },
...              "required": [
...                "email",
...                "gender",
...                "id",
...                "name",
...                "status"
...              ]
...            },
...            {
...              "type": "object",
...              "properties": {
...                "email": {
...                  "type": "string"
...                },
...                "gender": {
...                  "type": "string"
...                },
...                "id": {
...                  "type": "integer"
...                },
...                "name": {
...                  "type": "string"
...                },
...                "status": {
...                  "type": "string"
...                }
...              },
...              "required": [
...                "email",
...                "gender",
...                "id",
...                "name",
...                "status"
...              ]
...            },
...            {
...              "type": "object",
...              "properties": {
...                "email": {
...                  "type": "string"
...                },
...                "gender": {
...                  "type": "string"
...                },
...                "id": {
...                  "type": "integer"
...                },
...                "name": {
...                  "type": "string"
...                },
...                "status": {
...                  "type": "string"
...                }
...              },
...              "required": [
...                "email",
...                "gender",
...                "id",
...                "name",
...                "status"
...              ]
...            },
...            {
...              "type": "object",
...              "properties": {
...                "email": {
...                  "type": "string"
...                },
...                "gender": {
...                  "type": "string"
...                },
...                "id": {
...                  "type": "integer"
...                },
...                "name": {
...                  "type": "string"
...                },
...                "status": {
...                  "type": "string"
...                }
...              },
...              "required": [
...                "email",
...                "gender",
...                "id",
...                "name",
...                "status"
...              ]
...            },
...            {
...              "type": "object",
...              "properties": {
...                "email": {
...                  "type": "string"
...                },
...                "gender": {
...                  "type": "string"
...                },
...                "id": {
...                  "type": "integer"
...                },
...                "name": {
...                  "type": "string"
...                },
...                "status": {
...                  "type": "string"
...                }
...              },
...              "required": [
...                "email",
...                "gender",
...                "id",
...                "name",
...                "status"
...              ]
...            }
...          ]
...       }
    ${schema}=    Evaluate    json.loads('''${schema_str}''')       json
    Set Suite Variable    ${schema}
    Tc-logger    ${schema}